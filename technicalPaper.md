# Browser APIs for Storage.

## API - Application programming interface.

   API is a set of definitions and protocols for building and integrating application software. i.e., constructs are made available in programming languages which helps developers in creating complex functionalities easily.

   There are a huge number of APIs available that allows us to do wide-variety of things, one of them is "Browser APIs".

## Browser APIs 
   Browser API as the name suggests, is an API over the web which can be accessed using HTTP protocol. It is a concept not a technology. They are built into our web browser and are able to expose data from the browser and surrounding computer environment and do complex things.

## Browser APIs for storage
   The Browser storage API provides mechanisms by which browsers can store key-value pairs in a much more built-in fashion.

   Modern web browsers support many ways for websites to store data on the user's computer with permission, then retrieve it when necessary. This remains in the storage for offline usage and more.

   They can also be called client side storage.

  There are different kinds of mechanisms within Web Storage, few of them are as follows, 

* **Session storage :**
    It is a tab specific and scoped to the life time of the tab. It may be useful for storing small amount of session specific information and data is never transferred to the server for example an indexedDb key. It should be used with caution because it is synchronous and will block the main thread. it is limited to few MB. because it is tab specific, it is not accessible from web workers.


* **Local storage :**
    This does the same things but data remains when the browser is closed and reopened. Stores data with expiration date, and gets cleared only through JavaScript, or clearing the browser cache/locally stored data.

* **cookies :**
    Since the early days of the web, sites have used cookies to store information to personalize user experience on the website. They are the easiest way of storing data at client side.

* **File System and FileWriter API**
    They provide methods for reading and writing files to a file system, while it is asynchronous, it is not recommended because it is only available in Chromium-based browsers. 
 
* **Cache API**
    Some modern browsers support the new cache API. this API is designed for storing HTTP responses to specific requests, and is very useful for doing thing like storing websites properties offline so the site can subsequently be used without a network connection. Cache is usually used in combination with the Service worker API.


## Uses

* Personalized site preferences( ex. showing a user's choice of custom widgets, colour, scheme, or font size)
* Persisting previous site activity(ex. storing the contents of a shopping cart from a previous session etc...)
* Saving data and properties locally so a site will be quicker to download or be usable without a network connection.